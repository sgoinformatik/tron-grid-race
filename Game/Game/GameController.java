package Game;

import Game.Model.Player;
import Game.Model.PlayingField;
import Game.Threads.MovementTimer;
import Game.View.View;

/**
 * Initialize the game
 * @author tehrengruber
 */
public class GameController {
	/*
	 * Models
	 */
	private Player[] players;
	
	PlayingField field;
	
	/*
	 * Views
	 */
	View view;
	
	public GameController () {
		System.out.println("Initializing Tron Grid Race");
		
		// Init game
		this.initGame();
		
		// Init view
		this.initView();
	}
	
	/**
	 * Initialize view
	 */
	private void initView() {
		this.view = new View(this);
	}
	
	/**
	 * Initialize all models
	 */
	private void initGame() {
		// Create playing field
		// TODO: use appropriate size
		this.field = new PlayingField(100, 100);
		
		// Create players (currently only 2)
		this.players = new Player[2];
		this.players[0] = new Player("Player 1");
		this.players[1] = new Player("Player 2");
		
		// Create movement timer
		//(new MovementTimer(this.getPlayers())).start();
	}

	public static void main (String[] args) {
		new GameController();
	}

	public Player[] getPlayers() {
		return players;
	}

	public void setPlayers(Player[] players) {
		this.players = players;
	}
}
