package Game.Model;

public class PlayingField {
	/**
	 * Contains the trace of all players
	 */
	Boolean[][] grid;
	
	/**
	 * Construct class
	 * @param x size
	 * @param y size
	 * TODO implement
	 */
	public PlayingField (int x, int y) {
		
	}
	
	/**
	 * Set trace (player can not pass any coordinates that are already traced)
	 * 
	 * (needs to be synchronized cause we are working with threads)
	 * TODO throw exception if coordinates are already traced
	 * @param x
	 * @param y
	 */
	public synchronized void setTrace (int x, int y) {
	}
}
