package Game.View;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import Game.GameController;
import Game.Model.Player;
import Game.Threads.KeyboardControlThread;
import Objected2D.Objected2DExtended;
import Objected2D.Objected2DWindow;
import Objected2D.objects.Image;
import Objected2D.objects.Rect;


public class View {
	GameController controller;

	
	Objected2DExtended drawingArea;
	private int direction;
	private int spieler=2;
	
	public View(GameController c) {
		this.controller = c;
		
		// Initialize keyboard control thread
		new KeyboardControlThread(c.getPlayers());
		
		// Initialize objected2D
		Objected2DWindow window = new Objected2DWindow(900, 600, true);
    	
    	//Get object
    	this.drawingArea = window.getObjected2DExtended();
    	
    	this.drawingArea.addObject(new Rect(0, 0, 10, 10, true, Color.RED));
    	
    	//Implementing & Scaling Images
    	
    	Image Spielfeld=new Image("/home/students/druxren/TronGridRace/Spielfeld.png");
    	Spielfeld.scaleToWidth(900);
    	this.drawingArea.addObject(Spielfeld);
    	Spielfeld.inView();
    	
    	Image Spieler0=new Image("/home/students/druxren/TronGridRace/bikeskaliert1.png");
    	Spieler0.scaleToWidth(60);
    	this.drawingArea.addObject(Spieler0);
    	Spieler0.inViewX();
    	
    	Image Spieler1=new Image("/home/students/druxren/TronGridRace/bikeskaliert2.png");
    	Spieler1.scaleToWidth(60);
    	this.drawingArea.addObject(Spieler1);
    	Spieler1.inViewY();
    		
    	Image Spieler1Gewinnt=new Image("/home/students/druxren/TronGridRace/Spieler1gewinnt.png");
    	Spieler1Gewinnt.scaleToWidth(900);
    	this.drawingArea.addObject(Spieler1Gewinnt);
    	Spieler1Gewinnt.setHidden();
    	
    	Image Spieler2Gewinnt=new Image("/home/students/druxren/TronGridRace/Spieler2gewinnt.png");
    	Spieler2Gewinnt.scaleToWidth(900);
    	this.drawingArea.addObject(Spieler2Gewinnt);
    	Spieler2Gewinnt.setHidden();
    	
    	Image Startbild1=new Image("/home/students/druxren/TronGridRace/Startbild1.png");
    	Startbild1.scaleToWidth(900);
    	this.drawingArea.addObject(Startbild1);
    	Startbild1.setHidden();
    	
    	Image Startbild2=new Image("/home/students/druxren/TronGridRace/Startbild2.png");
    	Startbild2.scaleToWidth(900);
    	this.drawingArea.addObject(Startbild2);
    	Startbild2.setHidden();     	   	    	
    	
    	this.drawingArea.addObject(new Rect(0, 20, 10, 10, true, Color.RED));
    	
    	//getting Players and set Images to position
    	Player[] player=c.getPlayers();
    	Spieler0.moveTo(player[0].getPosition().width, player[0].getPosition().height);
    	Spieler1.moveTo(player[1].getPosition().width, player[1].getPosition().height+100);
    	
    	this.drawingArea.addObject(new Rect(50, 10, 10, 10, true, Color.RED));
    	
    	Spieler0.setVelocity(100);
		Spieler1.setVelocity(10);
		
    		//change Direction by command
    		if(spieler==0)
    		{
    			if(!(Spieler0.getDirection()==direction))
    			{
    				Spieler0.changeDirectionTo(direction);
    				spieler=2;
    				direction=400;
    			}   			
    		}
    		
    		if(spieler==1)
    		{
    			if(!(Spieler1.getDirection()==direction))
    			{
    				Spieler1.changeDirectionTo(direction);
    				spieler=2;
    				direction=400;
    			}   			
    		}
    		
    		// Debug
        	this.drawingArea.render();
        	window.repaint();
    	}
	
	
	//Possibility to turn Direction of Players
	public void moveUp(int pSpieler)
	{
		spieler=pSpieler;
		direction=270;
	}
	public void moveDown(int pSpieler)
	{
		spieler=pSpieler;
		direction=90;
	}
	public void moveRight(int pSpieler)
	{
		spieler=pSpieler;
		direction=0;
	}
	public void moveLeft(int pSpieler)
	{
		spieler=pSpieler;
		direction=180;
	}
	
	
}
