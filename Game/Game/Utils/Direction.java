package Game.Utils;

public enum Direction {
	NORTH,
	SOUTH,
	WEST,
	EAST
}
