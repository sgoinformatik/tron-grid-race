/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Objected2D;
import java.awt.Color;

import javax.swing.*;

import Objected2D.objects.Line;
import Objected2D.objects.Rect;

/**
 *
 * @author Till Ehrengruber <darthmatch@googlemail.com>
 * @copyright  2011 Till Ehrengruber <darthmatch@googlemail.com>
 */
public class Objected2DExample extends javax.swing.JPanel {
    public Objected2DExample () {
        super(true);
    }
    
    public static void main (String[] args) {
    	//Create Window
    	Objected2DWindow window = new Objected2DWindow(600, 600, true);

    	//Get object
    	Objected2DExtended objected2D = window.getObjected2DExtended();

    	// Create background
    	//Line[][] grid  = new Line[32][20];
    	
    	// Horizontal
    	for (int i=0;i<32; i++) {
    		Line l = new Line(0, i*20, 640, i*20+1,Color.red);
    		objected2D.addObject(l);
    	}
    	
    	// Vertical
		for (int j=0; j<20; j++) {
			
		}
    	
    	//Line line 
    	
    	//Create Rectangle
    	Rect rect1 = new Rect(0, 0, 200, 200);
    	Rect rect2 = new Rect(300, 300, 200, 200);

    	//Add Object
    	objected2D.addObject(rect1);
    	objected2D.addObject(rect2);
    	
    	//rect1.changeDirectionTo(45);	//Rotate rect
    	//rect1.setVelocity(40);

    	while (true) {
    		window.windowFrame.repaint();
    		
    		if (rect1.isCollidingWith(rect2)) {
    			rect2.setColor(Color.RED);
    		}
    		//System.out.println(rect1.isCollidingWith(rect2));
    		/*try {
				Thread.sleep(300);
			} catch (InterruptedException e) {}*/
    	}
    } 
}
